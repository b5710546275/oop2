# Lab1: Fundamentals Practice

In this lab you will work on 3 things:

1. How to write equals and why it should be declared as `equals(Object other)	`.
2. Writing Javadoc in classes.
3. Using Git and Bitbucket.

## Files for this lab.

1. In your workspace directory create a project named `lab1`.
2. Go to https://bitbucket.org/skeoop/oop/src.  In the **week1** directory download lab1.zip.
3. Unzip the file into your `lab1` project.

## Exploring Equals

1. Run the Main class in the lab1 project.
2. equals does not behave the way it should. (discuss)
3. Look at the "fundamental methods" document (online) for the correct way to write equals and implement it.

## Writing Javadoc

1. Write good Javadoc in the Student class.
2. Fix the equals method in Student.



