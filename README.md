# Object-Oriented Programming

This repository contains course materials for Object-Oriented Programming 2 at [Kasetsart University](http://www.ku.th).

[Wiki](https://bitbucket.org/skeoop/oop/wiki) contains the [course description](https://bitbucket.org/skeoop/oop/wiki/About) and [weekly schedule](https://bitbucket.org/skeoop/oop/wiki/Home).

[Source](https://bitbucket.org/skeoop/oop/src/master) repository has lecture slides, assignments, and related material.     
    [week1](https://bitbucket.org/skeoop/oop/src/master/week1) for week1 material,
    [week2](https://bitbucket.org/skeoop/oop/src/master/week2) for week2 material,
    and so on

[Results & Feedback](https://bitbucket.org/skeoop/oop/src/master/results) "results" folder in repository has results and feedback on assignments.